from abc import abstractmethod
from fastapi.responses import StreamingResponse
import requests, json
import logging
from typing import List
import threading
import httpx

stop_words = [
    "<|im_end|>",
    "[End]",
    "[end]",
    "\nReferences:\n",
    "\nSources:\n",
    "End.",
]


#
class LLM:
    def __init__(self):
        pass

    @abstractmethod
    def completion(
        self, model, messages=[], max_tokens=1024, temperature=1, top_p=1, n=1
    ):
        raise Exception("not yet implemented")


class Ollama(LLM):
    def __init__(self, base_url="http://localhost:11434/v1", **kwargs):
        super().__init__()
        self.base_url = base_url

    def local_client(self):
        import openai

        thread_local = threading.local()
        try:
            return thread_local.client
        except AttributeError:
            thread_local.client = openai.OpenAI(
                # USING OLLAMA
                base_url=self.base_url,
                api_key="ollama",
                timeout=httpx.Timeout(connect=10, read=120, write=120, pool=10),
            )
            return thread_local.client

    def completion(
        self,
        model="mistral",
        messages=[],
        max_tokens=1024,
        temperature=1,
        top_p=1,
        n=1,
        tools=[],
        stream=False,
        **kwargs,
    ):
        client = self.local_client()
        return client.chat.completions.create(
            model=model,
            messages=messages,
            max_tokens=max_tokens,
            stop=stop_words,
            stream=stream,
            temperature=temperature,
            tools=tools,
        )

    def stream_response(self, llm_response):
        for chunk in llm_response:
            if chunk.choices:
                yield chunk.choices[0].delta.content or ""


class Gemini(LLM):
    """A simple Gemini wrapper to call Gemini models"""

    def __init__(self, key) -> None:
        self.key = key

    def string_formatting(self, message):
        if isinstance(message, str):
            return message
        elif isinstance(message, dict):
            return "\n".join([f"{k}:{v}" for k, v in message.items()])
        else:
            assert f"Unsupported type {type(message)} found"

    def completion(
        self,
        model="gemini-pro",
        messages=[],
        tools=[],
        max_tokens=1024,
        temperature=1,
        top_p=1,
        n=1,
        stream=False,
        **kwargs,
    ):
        data = {
            "contents": [
                {
                    "parts": [
                        {"text": self.string_formatting(message)}
                        for message in messages
                    ]
                }
            ],
            # "":"",
            "generationConfig": {
                # "stopSequences": [
                #   string
                # ],
                "candidateCount": 1,
                "maxOutputTokens": max_tokens,
                "temperature": temperature,
                # "topP": 1,
                "topK": 1,
            },
        }
        headers = {
            "Content-Type": "application/json",
        }

        if len(tools):
            data["tools"] = {"function_declarations": [t["function"] for t in tools]}

        if stream:
            method = "streamGenerateContent"
        else:
            method = "generateContent"

        url = (
            f"https://generativelanguage.googleapis.com/v1beta/models/{model}:{method}"
        )

        response = requests.post(
            url, headers=headers, json=data, params={"key": self.key}
        )
        response.raise_for_status()

        return response

        # return StreamingResponse(
        #     self.stream_response(response),
        #     media_type="text/html",
        # )

    def stream_response(self, response):
        for result in self._raw_stream_response(response):
            yield result

    def _raw_stream_response(self, response):
        loaded_response = json.loads(response.text)
        if isinstance(loaded_response, List):
            for chunk in loaded_response:
                try:
                    ret = chunk["candidates"][0]["content"]["parts"][0]["text"]
                    yield ret.strip()
                except Exception as e:
                    logging.error(f"{e} with input {response.text}")
                    yield ""
        else:
            ret = loaded_response["candidates"][0]["content"]["parts"][0]["text"]
            yield ret
