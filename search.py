import requests, os, json, yaml
from gnews import GNews

from loguru import logger
from utils import logging_time

from llama_index.readers.web import TrafilaturaWebReader
from llm import *

DEFAULT_SEARCH_ENGINE_TIMEOUT = 5
REFERENCE_COUNT = 10
BING_SEARCH_V7_ENDPOINT = "https://api.bing.microsoft.com/v7.0/search"
BING_MKT = "en-US"
GOOGLE_SEARCH_ENDPOINT = "https://customsearch.googleapis.com/customsearch/v1"
SERPER_SEARCH_ENDPOINT = "https://serpapi.com/search"
SEARCHAPI_SEARCH_ENDPOINT = "https://www.searchapi.io/api/v1/search"

BING_SEARCH_V7_SUBSCRIPTION_KEY = os.getenv("BING_SEARCH_V7_SUBSCRIPTION_KEY")
SERPER_SEARCH_API_KEY = os.getenv("SERPER_SEARCH_API_KEY")

GOOGLE_SEARCH_API_KEY = os.getenv("GOOGLE_SEARCH_API_KEY")
GOOGLE_SEARCH_CX = os.getenv("GOOGLE_SEARCH_CX")

SEARCHAPI_SEARCH_API_KEY = os.getenv("SEARCHAPI_SEARCH_API_KEY")


from llama_index_cfg import llama_index_search

QUERY_OPT_PROMPT = """
Create google search term based on instruction below:

When a date or time mentioned, use that data in the following syntax. after:2020-03-23 before:2020-12-31
if no time mentioned, don't use it.

From now on, also come up with variations of what I search for, so with carbon dioxide reduction, go for CO2 reduction or phrases like that too.

Search documents include pdfs and html pages. Put that at end of each query each time in format as "filetype:(pdf OR html)".

If I use any nationality in my search term, don't use the word but site:(domain name). Example. Greek would be site:gr.

Example format:
Question: USER_INPUT
Answer: SEARCH_TERM

Let's try this now:
Question: {context}
Answer:
"""


class SearchEngine:
    def __init__(
        self,
        endpoint,
        params_builder,
        index_search=False,
        search_opitmize=False,
        **kwargs,
    ):
        self.endpoint = endpoint
        self.params_builder = params_builder
        self.index_search = index_search
        self.search_optimize = search_opitmize

    @logging_time
    def core_search(self, query):
        params = self.params_builder(query)
        headers = self.build_headers()
        try:
            response = requests.get(
                self.endpoint,
                headers=headers,
                params=params,
                timeout=DEFAULT_SEARCH_ENGINE_TIMEOUT,
            )
            response.raise_for_status()
            return self.parse_response(response.json())
        except (requests.RequestException, KeyError) as e:
            logger.error(f"Error encountered: {e}")
            return []

    def search_refinement(self, query, llm, llm_model):
        if not self.search_optimize:
            return query

        from search_with_lepton import stop_words

        # llm_response = llm_client.chat.completions.create(
        llm_response = llm.completion(
            model=llm_model,
            messages=[
                {"role": "system", "content": QUERY_OPT_PROMPT.format(context=query)}
            ],
            max_tokens=64,
            stop=stop_words,
            stream=False,
            temperature=1,
        )
        if isinstance(llm, Gemini):
            refined_query = llm_response.json()["candidates"][0]["content"]["parts"][0][
                "text"
            ]
        else:
            refined_query = (
                llm_response.choices[0].message.content.strip().replace('"', "")
            )
        # take the first line to avoid case that provide "variations" in a different line
        refined_query = refined_query.split("\n")[0]
        logger.info(f"Refined Query as: {refined_query}")
        return refined_query

    @logging_time
    def search(self, query, llm_client, llm_model):
        # should the query be transformed
        # instruct

        # if the system has enough information, no search
        if self.index_search:
            nodes = llama_index_search(query, [])
            if len(nodes) > 1:
                return nodes

        # if not perform full cycle
        # retry refinement for 3 times
        retry_times = 0
        feeds = []
        while retry_times <= 3:
            core_search_query = self.search_refinement(query, llm_client, llm_model)
            feeds = self.core_search(core_search_query)
            if len(feeds) > 0:
                break
            retry_times += 1

        if len(feeds) == 0:
            feeds = self.core_search(query)

        logger.info(f"{len(feeds)} feeds found")
        if self.index_search:
            # should the query for result be transformed
            return llama_index_search(query, feeds)
        else:
            return feeds

    def build_headers(self):
        return {"Content-Type": "application/json"}

    def parse_response(self, json_content):
        raise NotImplementedError


def bing_params_builder(query):
    return {"q": query, "mkt": BING_MKT}


class BingSearchEngine(SearchEngine):
    def __init__(self, **kwargs):
        super().__init__(BING_SEARCH_V7_ENDPOINT, bing_params_builder, **kwargs)

    def build_headers(self):
        h = super().build_headers()
        h["Ocp-Apim-Subscription-Key"] = BING_SEARCH_V7_SUBSCRIPTION_KEY
        return h

    def parse_response(self, json_content):
        try:
            return json_content["webPages"]["value"][:REFERENCE_COUNT]
        except KeyError:
            return []


def google_params_builder(query):
    return {
        "key": GOOGLE_SEARCH_API_KEY,
        "cx": GOOGLE_SEARCH_CX,
        "q": query,
        "num": REFERENCE_COUNT * 5,
    }


class GoogleSearchEngine(SearchEngine):
    def __init__(self, **kwargs):
        super().__init__(GOOGLE_SEARCH_ENDPOINT, google_params_builder, **kwargs)

    def parse_response(self, json_content):
        try:
            contexts = json_content["items"][:REFERENCE_COUNT]
            for c in contexts:
                c["url"] = c["link"]
            return contexts
        except KeyError:
            return []


def searchapi_params_builder(query):
    return {
        "q": query,
        "engine": "google",
        "num": (
            REFERENCE_COUNT
            if REFERENCE_COUNT % 10 == 0
            else (REFERENCE_COUNT // 10 + 1) * 10
        ),
    }


class SearchAPISearchEngine(SearchEngine):
    def __init__(self, **kwargs):
        super().__init__(SEARCHAPI_SEARCH_ENDPOINT, searchapi_params_builder, **kwargs)

    def build_headers(self):
        h = super().build_headers()
        h["Authorization"] = f"Bearer {SEARCHAPI_SEARCH_API_KEY}"
        return h

    def parse_response(self, json_content):
        try:
            # convert to the same format as bing/google
            contexts = []

            if json_content.get("answer_box"):
                if json_content["answer_box"].get("organic_result"):
                    title = (
                        json_content["answer_box"]
                        .get("organic_result")
                        .get("title", "")
                    )
                    url = (
                        json_content["answer_box"].get("organic_result").get("link", "")
                    )
                if json_content["answer_box"].get("type") == "population_graph":
                    title = json_content["answer_box"].get("place", "")
                    url = json_content["answer_box"].get("explore_more_link", "")

                title = json_content["answer_box"].get("title", "")
                url = json_content["answer_box"].get("link")
                snippet = json_content["answer_box"].get("answer") or json_content[
                    "answer_box"
                ].get("snippet")

                if url and snippet:
                    contexts.append({"name": title, "url": url, "snippet": snippet})

            if json_content.get("knowledge_graph"):
                if json_content["knowledge_graph"].get("source"):
                    url = json_content["knowledge_graph"].get("source").get("link", "")

                url = json_content["knowledge_graph"].get("website", "")
                snippet = json_content["knowledge_graph"].get("description")

                if url and snippet:
                    contexts.append(
                        {
                            "name": json_content["knowledge_graph"].get("title", ""),
                            "url": url,
                            "snippet": snippet,
                        }
                    )

            contexts += [
                {"name": c["title"], "url": c["link"], "snippet": c.get("snippet", "")}
                for c in json_content["organic_results"]
            ]

            if json_content.get("related_questions"):
                for question in json_content["related_questions"]:
                    if question.get("source"):
                        url = question.get("source").get("link", "")
                    else:
                        url = ""

                    snippet = question.get("answer", "")

                    if url and snippet:
                        contexts.append(
                            {
                                "name": question.get("question", ""),
                                "url": url,
                                "snippet": snippet,
                            }
                        )

            return contexts[:REFERENCE_COUNT]
        except KeyError:
            logger.error(f"Error encountered: {json_content}")
            return []


class GoogleNewsEngine(SearchEngine):
    def __init__(self, **kwargs):
        super().__init__(None, None, **kwargs)

    def core_search(self, query):
        google_news = GNews(language="en", country="US", max_results=REFERENCE_COUNT)
        return self.parse_response(google_news.get_news(query))

    def parse_response(self, json_content):
        try:
            contexts = json_content[:REFERENCE_COUNT]
            for c in contexts:
                c["snippet"] = c["description"]
            return contexts
        except KeyError:
            return []


# Define other search engines similarly...
class SearchEngineFactory:
    @staticmethod
    def create_engine(engine_name: str, **kwargs) -> SearchEngine:
        if engine_name == "BING":
            return BingSearchEngine(**kwargs)
        elif engine_name == "GOOGLE":
            return GoogleSearchEngine(**kwargs)
        elif engine_name == "GNEWS":
            return GoogleNewsEngine(**kwargs)

        # elif engine_name == "LLAMA":
        #     return LlamaSearchEngine()
        elif engine_name == "searchapi":
            return SearchAPISearchEngine(**kwargs)
        else:
            raise ValueError("Invalid search engine name")


# Usage:
def main():
    query = "fed interest rate adjustment forecast"
    # bing_search_engine = BingSearchEngine()
    google_search_engine = GoogleNewsEngine()

    # contexts = bing_search_engine.search(query, BING_SEARCH_V7_SUBSCRIPTION_KEY)
    contexts = google_search_engine.search(query)

    print(json.dumps(contexts, indent=2))


if __name__ == "__main__":
    main()
