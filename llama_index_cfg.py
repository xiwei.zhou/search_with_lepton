from llama_index.core import Document
from llama_index.readers.web import TrafilaturaWebReader
from llama_index.core.retrievers import VectorIndexRetriever
from llama_index.core import VectorStoreIndex, SimpleDirectoryReader
from llama_index.vector_stores.chroma import ChromaVectorStore
from llama_index.core import StorageContext, QueryBundle
from llama_index.embeddings.huggingface import HuggingFaceEmbedding
from llama_index.core.postprocessor import LLMRerank, SentenceTransformerRerank
from llama_index.core.vector_stores.types import (
    MetadataInfo,
    MetadataFilters,
    MetadataFilter,
)
from llama_index.core import Settings
from loguru import logger
import chromadb
import random
from time import sleep, time
import json
from utils import logging_time
import concurrent.futures

# EMBEDDINGS = {
#     "OpenAI": OpenAIEmbedding(),
#     "bge-large": HuggingFaceEmbedding(model_name='BAAI/bge-large-en'), # You can use mean pooling by addin pooling='mean' parameter
#     "llm-embedder": HuggingFaceEmbedding(model_name='BAAI/llm-embedder'),
#     "CohereV2": CohereEmbedding(cohere_api_key=cohere_api_key, model_name='embed-english-v2.0'),
#     "CohereV3": CohereEmbedding(cohere_api_key=cohere_api_key, model_name='embed-english-v3.0', input_type='search_document'),
#     "Voyage": VoyageEmbeddings(voyage_api_key=voyage_api_key),
#     "JinaAI-Small": HuggingFaceEmbedding(model_name='jinaai/jina-embeddings-v2-small-en', pooling='mean', trust_remote_code=True),
#     "JinaAI-Base": HuggingFaceEmbedding(model_name='jinaai/jina-embeddings-v2-base-en', pooling='mean', trust_remote_code=True),
#     "Google-PaLM": GooglePaLMEmbeddings(google_api_key=google_api_key)
# }

# RERANKERS = {
#     "WithoutReranker": "None",
#     "CohereRerank": CohereRerank(api_key=cohere_api_key, top_n=5),
#     "bge-reranker-base": SentenceTransformerRerank(model="BAAI/bge-reranker-base", top_n=5),
#     "bge-reranker-large": SentenceTransformerRerank(model="BAAI/bge-reranker-large", top_n=5)
# }

Settings.embed_model = HuggingFaceEmbedding(
    model_name="jinaai/jina-embeddings-v2-base-en",
    pooling="mean",
    trust_remote_code=True,
)

reranker = SentenceTransformerRerank(model="BAAI/bge-reranker-base", top_n=8)
# HuggingFaceEmbedding(
#     model_name="sentence-transformers/all-mpnet-base-v2"
# )
Settings.chunk_size = 512

# initialize client, setting path to save data
db = chromadb.PersistentClient(path="./chroma_db")

# create collection
chroma_collection = db.get_or_create_collection("numlia-1")

# assign chroma as the vector_store to the context
vector_store = ChromaVectorStore(chroma_collection=chroma_collection)
storage_context = StorageContext.from_defaults(vector_store=vector_store)


# given a list of url
import requests
from gnews import GNews
import uuid


def has_this_doc(feed, field="url", **kwargs):
    index = VectorStoreIndex.from_vector_store(
        vector_store=vector_store, storage_context=storage_context
    )
    retriever = VectorIndexRetriever(
        index=index,
        similarity_top_k=1,
        filters=MetadataFilters.from_dict({field: feed[field]}),
    )
    return len(retriever.retrieve("*")) > 0


def search_with_gnews(query: str, feeds=None, max_results=8, full_doc=False):
    begin = time()
    logger.info(f"Search GNews for {query} at {begin}")
    google_news = GNews(language="en", country="US", max_results=max_results)
    rets = []
    feeds = google_news.get_news(query)
    for entry in feeds:
        entry["snippet"] = entry["description"]

        if full_doc:
            # check if url has been downloaded, if yes, continue
            if has_this_doc(entry, "title"):
                logger.info(f"entry {entry['title']} found in the index, skip it")
                continue

            sleep(random.randrange(1, 100) / 200)
            entry["link"] = get_original_link(entry["url"])

            # proxy will be
            logger.info(
                f"Downloading article {entry['title']} with link {entry['link']}"
            )
            article = google_news.get_full_article(entry["link"])
            if article:
                entry["text"] = article.text
                logger.info(
                    f"Download article {article.title} with link {article.url} succeed"
                )

        rets.append(entry)
    end = time()
    logger.info(
        f"finish {end-begin} seconds with {len(rets)}/{len(feeds)} documents success"
    )
    return rets


def get_original_link(redirect_url):
    try:
        with requests.Session() as session:
            response = session.head(redirect_url, allow_redirects=True, timeout=2)
            return response.url
    except:
        return redirect_url


# fetch document
from datetime import datetime


@logging_time
def get_documents(candidates, metadata=False):
    def get_publish_time(entry):
        keys = ["published date", "publish_date", "publish_time", "publish time"]
        for k in keys:
            try:
                return entry.get(k)
            except:
                logger.info(f"{k} not found")
        logger.error(f"no key found for the entry")
        return datetime.today()

    documents = []
    for candidate in candidates:
        if (
            candidate.get("link", "").find("msn") != -1
            or candidate.get("text", "") == ""
        ):
            logger.info(f"ignore {candidate}")
            continue
        try:
            # doc = TrafilaturaWebReader().load_data([candidate["link"]])[0]
            doc = Document(text=candidate["text"])
            if metadata:
                doc.metadata = {
                    "snippet": candidate["snippet"],
                    "title": candidate["title"],
                    "publish_time": get_publish_time(
                        candidate
                    ),  # candidate["published date"],
                    "link": candidate["link"],
                    "source_doc_id": str(
                        uuid.uuid5(uuid.NAMESPACE_URL, candidate["link"])
                    ),
                }
                # doc.doc_id = str(uuid.uuid5(uuid.NAMESPACE_URL, doc.doc_id))
            documents.append(doc)
        except Exception as e:
            logger.error(f"{str(e)} at {candidate}")

    return documents


import PyPDF2


# query = 'Why OpenAI Sora release is an important milestone to AGI'
class URLReader:
    def __init__(self, url):
        self.url = url
        self._content = None

    @property
    def content(self):
        if self._content is None:
            with requests.get(self.url) as response:
                response.raise_for_status()
                self._content = response.content
        return self._content

    def read_url(self):
        content_type = self.get_content_type()
        if content_type == "html":
            return self.read_html()
        elif content_type == "pdf":
            return self.read_pdf()
        else:
            return self.read_default()

    def get_content_type(self):
        content_type = requests.head(self.url).headers.get("content-type", "").lower()
        if "html" in content_type:
            return "html"
        elif "pdf" in content_type:
            return "pdf"
        else:
            raise ValueError(f"Unknown content type {content_type} found")

    def read_html(self):
        try:
            google_news = GNews()
            article = google_news.get_full_article(self.url)
            return article.text
        except Exception as e:
            a = TrafilaturaWebReader().load_data(urls=self.url)
            return a[0].text

    def read_pdf(self):
        pdf_text = ""
        with requests.get(self.url, stream=True) as response:
            reader = PyPDF2.PdfFileReader(response.raw)
            for page_num in range(reader.numPages):
                page = reader.getPage(page_num)
                pdf_text += page.extractText()
        return pdf_text

    def read_default(self):
        return self.content.decode("utf-8", "ignore")


def pull_document(entry):
    if has_this_doc(entry, "title"):
        logger.info(f"entry {entry['title']} found in the index, skip it")
    else:
        sleep(random.randrange(1, 100) / 200)
        entry["link"] = get_original_link(entry["url"])

        # proxy will be
        logger.info(f"Downloading article {entry['title']} with link {entry['link']}")

        try:
            url_reader = URLReader(entry["link"])
            text = url_reader.read_url()
            entry["text"] = text
            logger.info(f"Download article {text} with link {entry['link']} succeed")
        except Exception as e:
            entry["text"] = entry["snippet"]
            logger.info(f'{entry["link"]} not able to load')

    return entry


@logging_time
def pull_documents(feeds):
    with concurrent.futures.ThreadPoolExecutor() as executor:
        rets = list(executor.map(pull_document, feeds))
    return rets


@logging_time
def llama_index_search(query, feeds, topk=16):
    if len(feeds):
        # load documents
        docs = pull_documents(feeds)
        documents = get_documents(docs, metadata=True)
        candidates_urls = [doc["title"] for doc in docs]
        logger.info(f"create index from {len(documents)} documents")
        # save anything new, indexing them

        index = VectorStoreIndex.from_documents(
            documents, storage_context=storage_context
        )
        retriever = VectorIndexRetriever(
            index=index,
            similarity_top_k=topk,
            filters=MetadataFilters(
                filters=[
                    MetadataFilter(key="title", value=url) for url in candidates_urls
                ],
                condition="or",
            ),
        )
    else:
        index = VectorStoreIndex.from_vector_store(
            vector_store=vector_store, storage_context=storage_context
        )
        retriever = VectorIndexRetriever(
            index=index,
            similarity_top_k=topk,
        )

    return rag(query, retriever, reranker)


import spacy
from typing import List

nlp = spacy.load("en_core_web_sm")
STOP_WORDS = nlp.Defaults.stop_words


def preprocess_input(user_input: str) -> List[str]:
    return [
        token.text
        for token in nlp(user_input.lower())
        if token.is_alpha and token.text not in STOP_WORDS
    ]


def simple_stem(token: str) -> str:
    # Simple stemming by removing common suffixes
    suffixes = ["s", "es", "ed", "ing"]
    for suffix in suffixes:
        if token.endswith(suffix):
            token = token[: -len(suffix)]
    return token


def query_optimizer(user_input: str) -> str:
    tokens = preprocess_input(user_input)
    return " ".join(map(simple_stem, tokens))


# def query_optimizer(query):
#     # remove stop words
#     return query


@logging_time
def rag(query, retriever, reranker, threshold=0.5):
    logger.info(f"retrieve query: {query}")

    # QUERY can be optimized to remove stop words
    retriever_query = query_optimizer(query)

    retrieved_nodes = retriever.retrieve(retriever_query)

    retrieved_nodes = reranker.postprocess_nodes(
        retrieved_nodes, QueryBundle(retriever_query)
    )
    retrieved_nodes = [n for n in retrieved_nodes if n.score >= threshold]

    # RERANK
    logger.info(f"Retrive {len(retrieved_nodes)} records")

    # result back to main flow
    ret = [
        {
            "title": node.metadata["title"],
            "url": node.metadata["link"],
            "snippet": node.text,
            "score": node.score,
        }
        for node in retrieved_nodes
    ]
    logger.info(f"Retrived results: {json.dumps(ret, cls=NumpyEncoder, indent=2)}")
    return ret


import json
import numpy as np


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.float32):
            return float(obj)
        return json.JSONEncoder.default(self, obj)


def llama_summary_query(query, topk=10):

    feeds = search_with_gnews(query, full_doc=True)
    documents = get_documents(feeds, metadata=True)

    logger.info(f"create summary index from {len(documents)} documents")
    index = VectorStoreIndex.from_documents(documents, storage_context=storage_context)
    retriever = VectorIndexRetriever(
        index=index,
        similarity_top_k=topk,
    )

    logger.info(f"retrieve query: {query}")
    retrieved_nodes = retriever.retrieve(query)
    logger.info(f"Retrive {len(retrieved_nodes)} records")
    return [
        {
            "title": node.metadata["title"],
            "url": node.metadata["link"],
            "snippet": node.text,
        }
        for node in retrieved_nodes
    ]


# for node in retrieved_nodes:

#     print(node.get_content(NodeRelationship.SOURCE))
