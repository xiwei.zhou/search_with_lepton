import time
from functools import wraps
from loguru import logger


def logging_time(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        elapsed_time = end_time - start_time
        logger.info(
            f"Method {func.__name__} took {elapsed_time:.2f} seconds to execute."
        )
        return result

    return wrapper
